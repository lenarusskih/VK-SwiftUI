//
//  LoginViewInterface.swift
//  VK-SwiftUI
//
//  Created by Елена Русских on 2024-01-01.
//

// MARK: - LoginViewInterface
protocol LoginViewModelInterface {
    func logOut()
}
