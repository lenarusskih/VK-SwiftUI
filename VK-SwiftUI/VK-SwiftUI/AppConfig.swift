//
//  AppConfig.swift
//  VK-SwiftUI
//
//  Created by Елена Русских on 2024-01-06.
//

import Foundation

class AppConfig {
    static let isRunningInCI = ProcessInfo.processInfo.environment["IS_CI"] == "true"
}
