//
//  Extension+String.swift
//  VK-SwiftUI
//
//  Created by Елена Русских on 2024-01-03.
//

extension String {
    static var empty: String {
        ""
    }
    static var emptyCharacterString: String {
        " "
    }
}
