//
//  Extension+Character.swift
//  VK-SwiftUI
//
//  Created by Елена Русских on 2024-01-03.
//

extension Character {
    static var emptyCharacter: Character {
        " "
    }
}
