//
//  Extension+UserDefaults.swift
//  VK-SwiftUI
//
//  Created by Елена Русских on 2024-01-01.
//

import Foundation

// MARK: - UserDefaultsKeys
extension UserDefaults {
    enum Keys {
        static let isLogin = "isLogin"
    }
}
